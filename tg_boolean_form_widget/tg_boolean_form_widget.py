# encoding: utf-8

'''boolean_form_widget
'''

__author__ = 'Chris Miles'
__copyright__ = '(c) Chris Miles 2007'
__id__ = '$Id: tg_boolean_form_widget.py 324 2007-03-10 23:43:36Z chris $'
__url__ = '$URL: https://www.psychofx.com/psychofx/svn/Projects/Python/TurboGears/tg_boolean_form_widget/trunk/tg_boolean_form_widget/tg_boolean_form_widget.py $'
__version__ = '0.1'


# ---- Imports ----

# - TurboGears Modules -
from turbogears import widgets


# ---- Classes ----

class BooleanForm(widgets.Form):
    name = "booleanform"
    member_widgets = ["yes", "no"]
    params = ["action", "method", "form_attrs", "yes_text", "no_text"]
    params_doc = {'action' : 'Url to POST/GET the form data',
                  'method' : 'HTTP request method',
                  'form_attrs' : 'Extra (X)HTML attributes for the form tag',
                  'yes_text' : 'Text for the yes/confirm button',
                  'no_text' : 'Text for the no/cancel button',
                  'disabled_fields' : 'List of names of the fields we want to '
                                      'disable'}
    yes = widgets.SubmitButton('yes')
    no = widgets.SubmitButton('no')
    yes_text = None
    no_text = None
    
    template = """
    <form xmlns:py="http://purl.org/kid/ns#"
        name="${name}"
        action="${action}"
        method="${method}"
        class="booleanform"
        py:attrs="form_attrs"
    >
        <div py:for="field in hidden_fields" 
            py:replace="field.display(value_for(field), **params_for(field))" 
        />
        <div py:attrs="form_attrs">
            <div py:for="i, field in enumerate(fields)" 
                class="${i%2 and 'odd' or 'even'}"
            >
                <div>
                    <label class="fieldlabel" for="${field.field_id}" py:content="field.label" />
                </div>
                <div>
                    <span py:replace="field.display(value_for(field), **params_for(field))" />
                    <span py:if="error_for(field)" class="fielderror" py:content="error_for(field)" />
                    <span py:if="field.help_text" class="fieldhelp" py:content="field.help_text" />
                </div>
            </div>
            <div class="boolean_form_buttons">
                <div class="boolean_form_button_yes" py:content="yes.display(yes_text)" />
                <div class="boolean_form_button_no" py:content="no.display(no_text)" />
            </div>
        </div>
    </form>
    """
