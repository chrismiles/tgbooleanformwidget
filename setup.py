#!/usr/bin/env python

from ez_setup import use_setuptools
use_setuptools()
from setuptools import setup, find_packages

setup(
    name='TGBooleanFormWidget',
    version='0.1',
    description='A yes/no or confirm/cancel input widget for TurboGears projects.',
    long_description="""\
This module provides a BooleanForm class, which acts like widgets.Form, except that
it displays two submit buttons, one for confirming the action, the other for cancelling.
""",
    author='Chris Miles',
    author_email='miles.chris@gmail.com',
    url='http://www.psychofx.com/TGBooleanFormWidget/',
    license='MIT',
    packages=['tg_boolean_form_widget'],
    install_requires = ["TurboGears>=1.0.1"],
    zip_safe = False,
    keywords = [
        'turbogears.widgets',
    ],
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Framework :: TurboGears',
        'Framework :: TurboGears :: Widgets',
    ],
)
